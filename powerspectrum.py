import multiprocessing as mp
import numpy as np
import ls, errno, urllib
from os import environ as ENV

import astropy.io.fits as pyfits

"""
Use makelightcurve() to construct a light curve from KEPLER fits files, then
use spectrum() to compute the power spectrum.  
"""
fitskw='PDCSAP_FLUX'

def makelightcurve(fitskw, filelist = None, star = None, data_type = None, single_Q = False, download = False, savetxt=None, time0 = [],time0_lc = [],concatenated=False):
    """
       Construct a light curve from Kepler fits files. The function takes a list
       of file names, one for each quarter, extracts, reduces and concatenates the
       list of data given by the fits keyword fitskw. In Kepler fits files this is
       typically either SAP_FLUX or PDCSAP_FLUX. Note that the list of file names 
       can be URLs (untested, see path below). The data are cleaned for nan and 
       inf values, and the then converted to ppm.
       
       MAST archive path:
       http://archive.stsci.edu/missions/kepler/lightcurves/
    """
    
    if single_Q == True:
        time, data = {},{}
    else:
        time, data = np.array([]), np.array([])
    
    if star != None:
        filelist = get_kic_file_names(star, dat_type = data_type)

        if download :
            import astropy.utils.data as astrodata
            import os
            if not os.path.exists('./KIC'+star): 
                os.makedirs('./KIC'+star)

    for i,line in enumerate(filelist):    
        print line
        if star!= None:
            if download :
                file_dwnld='./KIC'+star+'/'+line.split('/')[-1]
                dummy=astrodata.download_file(line)
                os.system('cp '+dummy+' '+file_dwnld)
                fitsfile = pyfits.open(file_dwnld)
        else :
            fitsfile = pyfits.open(line)
    
        t, d = np.array(fitsfile[1].data.field('TIME')), np.array(fitsfile[1].data.field(fitskw))
        qual_flags  = np.array(fitsfile[1].data.field('SAP_QUALITY'))
        
        idx = qual_flags == 0
        t, d = t[idx], d[idx]
        sort_idx = np.array(np.isinf(d) | np.isnan(d))
        
        t, d = t[np.invert(sort_idx)], d[np.invert(sort_idx)]
    
        
        d = (d/np.median(d) - 1)*1e6
        
        if single_Q == True:
            Q = fitsfile[0].header['QUARTER']
            
            time['Q'+str(Q)],data['Q'+str(Q)] = t,d
            
        else:
            
            time, data = np.append(time, t),np.append(data, d)
   
        if not concatenated:
            time0.append(fitsfile[1].header['LC_START'])
        
            time0_lc.append(t[0])

        fitsfile.close()

    idx = np.argsort(time)
    
    time, data = time[idx],data[idx]
    
    data -= np.mean(data)
    
    if savetxt!= None:
        np.savetxt(savetxt,np.column_stack([time,data]))

    return time, data


def get_kic_file_names(star, dat_type = 'all'):
    
    if 'kplr' in star: star = star.strip('kplr')
    if 'KIC' in star: star = star.strip('KIC')
    
    if dat_type == 'llc': 
        ext = '_llc.fits'
    elif dat_type == 'slc':         
        ext = '_slc.fits'
    else:        
        ext = '.fits'
     
    url = 'http://archive.stsci.edu/missions/kepler/lightcurves/%s/%s' % (star[:4],star)

    url_list = urllib.urlopen(url).read().split('light curve file')
    
    file_list = []
    for i,line in enumerate(url_list):
        idx = line.index('kplr'+star)
        if '.tar' in line: continue
        filename = line[idx:idx+36]
        
        if ext in filename: file_list.append(url+'/'+filename)
    return file_list





def for_fortran(task, f, out, d, t, w, res):
    """The job sent to each worker. The indices st and sl for the frequency range 
    is taken by the worker, and the powerspectrum is then computed in this range
    by using either the ls_wo_w (without weights) or the ls_w_w (with weights)
    functions from ls.f90."""
    st, sl = task.get()

    freq = f[st:sl]
    
    if len(w) == 0:
        out[st:sl,0],out[st:sl,1],out[st:sl,2] = ls.ls_wo_w(t, d, freq)
    
    if len(w)==len(d):
        out[st:sl,0],out[st:sl,1],out[st:sl,2] = ls.ls_w_w(t, d, w, freq)

    res.put(out)

def setup_workers(f, d, t, w, out, n):
    """Initializes the workers and divides the frequency range into chunks that
    that are then handed to each worker. The result res is then joined when
    each worker reports it is finished."""


    if n==1 :
        print '1-cpu branch selected.'
        if len(w) == 0:
            out[:,0],out[:,1],out[:,2] = ls.ls_wo_w(t, d, f)
    
        if len(w)==len(d):
            out[:,0],out[:,1],out[:,2] = ls.ls_w_w(t, d, w, f)
            
	return out

    res, task =mp.Queue(), mp.Queue()

    if n> mp.cpu_count(): n=mp.cpu_count()  

    print 'number of child processess:', n
        
    L = int(len(f)/n)
   
    workers = [mp.Process(target=for_fortran, args=(task, f, out, d, t, w, res)) for i in xrange(n)]

    for each in workers: each.start() 
        
    for i in xrange(len(workers)):
            
        if i == len(workers)-1:
            task.put([i*L,len(f)-1])
        else:
            task.put([i*L,(i+1)*L])

    task.close()
    task.join_thread()
        
    i = 0
    while n:
        try:
            out += res.get()
        except IOError, e:
            if e.errno == errno.EINTR:
                continue
            else:
                raise
        n -=1
        i+=1
    
    return out

def winfunc(time, freq = None, fstart = None, fend = None, df = None, osample = 1, n = 1, weight = np.array([]),freq_mean=None):
    """Computes the window function of a specific time series."""    
    
    time -= time[0]
    
    if df is None: 
        df = 1./(int(osample)*time[-1])
                
    if fstart is None:
        fstart = 1./time[-1]
        
    if fend is None:
        fend = 1./(2*np.median(np.diff(time)))
           
    if freq is None:    
        f = np.arange(fstart,fend,df)*2*np.pi
        
    else:
        f = freq
    if freq_mean is None : 
        freq_mean = np.mean(f/2/np.pi)
        

    cos_win = np.cos(2*np.pi*freq_mean*time)

    sin_win = np.sin(2*np.pi*freq_mean*time)    
    
    outcos = setup_workers(f, cos_win, time, weight, np.zeros([len(f),3]), n)  #Calculating the powerspectrum  

    outsin = setup_workers(f, sin_win, time, weight, np.zeros([len(f),3]), n)  #Calculating the powerspectrum  

    pwin = 0.5*(outcos[:,2]+outsin[:,2])
    
    return pwin, f

    
def PSD(p, f_w, window):
    """Converts the powerspectrum into power spectral density."""
    return p/np.trapz(window, f_w)


def spectrum(t_i, d, psd = False, freq = None, fstart = None, fend = None, df = None, w = np.array([]), osample = 1, n = 1, f = None, days = True, try_if_wrong_freq = True, out_file = '',to_microHz = False):
    """For a time series consisting of measurements d at times t (required inputs), 
	computes the Lomb-Scargle (LS) periodogram p at a given set of test frequencies f.

	An array of test frequencies can be given, or the start frequency fstart, end 
	frequency fend and resolution res can be specified. If no frequencies or limits
	are given the default values are fstart = 1/T, fend = Nyquist frequency and res = 1/T.
	osample specifies the factor with which to oversample the frequencies.


	Also computes and returns the arrays a and b which contain the phase information. 

	The LS is normalized to minimize the effect of RMS noise from the time series.
 
	Multiprocessing is used if the option n is greater than 1. 

	The power spectral density PSD can also be computed. The window function is computed
	and the power spectrum is divided by the integral of the window function i.e., the 
	effective length of the time series.

        CHANGES:
          21-Mar-2016 (E.P.):
            Input time is supposed to be in days. If that's not the case, 
            please set days = False when calling the function.
             
            if an array of frequencies is given via the freq = ... keyword, then
            a warning is printed if such array is incompatible with the lightcurve.
            However if try_if_wrong_freq is set to True, than the warning is converted to
            an Error, and the function returns -1.

    N.B. whatever is the input, the code assume/change the frequencies such that the 
         time array used in the transform is in days.
         Conversion to Hz is done at the end.
	""" 

    t = t_i[:] - t_i[0]
    
    #if days : t *= 86400.

    if df    is None: df     = 1./(int(osample)*t[-1]) 
    
    if fstart is None: fstart = 1./t[-1]
        
    if fend   is None: fend   = 1./(2*np.median(np.diff(t)))
    
    if freq is None:    
        f = np.arange(fstart,fend,df)
    else:
        print 'using given frequencies...'
        if len(np.arange(fstart,fend,df)) < len(freq):
            if try_if_wrong_freq :
               print 'WARNING! Input frequencies incompatible with lightcurve.'
            else :
               print 'ERROR! Input frequencies incompatible with lightcurve.'
               return -1
        if days :
            print 'Input frequencies assumed to be in Hz'
            f = freq*86400.  #convert to days**-1
        else :
            f = freq

    print '\n'
    print 'PSD: ',psd 
    
    if days : 
        print 'Total time series length: ',t[-1],' days'
        print 'Resolution: ',df, ' days'
        if psd : 
            if to_microHz :
                print(r'Output will be in $\mu$Hz and ppm^2/$\mu$Hz')
            else :
                print 'Output will be in Hz and ppm^2/Hz'
        else : 
            if to_microHz :
                print(r'Output frequencies will be in $\mu$Hz')
            else :
                print 'Output frequencies will be in Hz'
    else : 
        print 'Total time series length: ',t[-1], ' input time units'
        print 'Resolution: ',df, ' 1/input time units'

    print 'Number of test frequencies: ',len(f)
    
    f *= 2*np.pi
    
    out = setup_workers(f, d, t, w, np.zeros([len(f),3]), n)  
    
    a, b, p = out[:,0], out[:,1], out[:,2]  
    
    if psd == True:

        window, f_w = winfunc(t, freq = f, n = n)
    
        p = PSD(p, f_w, window)

        #converting output to Hz and ppm^2/Hz
        if days :
            p*= 86400*2*np.pi 
            f/= 86400.
            if to_microHz :
                f, p, a, b = PSD_Hz_to_muHz(f, p, Re_I = a, Im_I = b)

        if out_file != '' : np.savetxt(out_file,np.column_stack([f/2/np.pi, p, a, b, window ]))

        return f/2/np.pi, p, a, b, window
    else:
        #converting output to Hz and ppm^2/Hz
        if days :
            f/= 86400.
            if to_microHz : f*=1e6

        if out_file != '' : np.savetxt(out_file,np.column_stack([f/2/np.pi, p, a, b]))
        return f/2/np.pi, p, a, b


def PSD_Hz_to_muHz(f,psd, Re_I = None, Im_I = None) :
    "Convert psd from ppm^2/Hz to ppm^2/microHz and freq from Hz to microHz"

    if (Re_I is not None) and (Im_I is not None):
        return f*1e6, psd*1e-6, Re_I*1e-3, Im_I*1e-3
    else:
        return f*1e6, psd*1e-6

def PSD_days_to_muHz(f,psd,Re_I = None, Im_I = None) :
    "Convert psd from ppm^2/days^-1 to ppm^2/microHz and freq from days^-1 to microHz"

    if (Re_I is not None) and (Im_I is not None):
        return f*86400e6, psd*86400e-6, Re_I*np.sqrt(86400.)*1e-3, Im_I*np.sqrt(86400.)*1e-3
    else :
        return f/86400e-6, psd*86400e-6

def bandpass_filter(T,f,a,b,f1,f2, win=None, n = 1):
    
    if f1 >= f2:
        raise ValueError('f2 must be greater than f1. Try again.')
        
    if win == None:
        win,fwin = winfunc(T, n = n)
        
    idx = (f > f1) - (f > f2)
    
    idx = idx.astype(bool)
          
    D = np.zeros_like(T)
    
    for i,t in enumerate(T):
        D[i] = sum(a[idx]*np.sin(2*np.pi*f[idx]*t)+b[idx]*np.cos(2*np.pi*f[idx]*t))
    D /= sum(win)
    D -= np.mean(D)    
    return D 

# ROUTINES ADDED BY Emanuele Papini

def get_lightcurve(filel, fitskw='FLUX'):
    """
        get lightcurve from single file.
        Use this procedure eg, if you are using a concatenated kepler lightcurve.

    """
    fitsfile=pyfits.open(filel[0])
    time, light= np.array(fitsfile[1].data.field('TIME')), np.array(fitsfile[1].data.field(fitskw))
    fitsfile.close()
    return time, light 

def get_concatenated_lightcurve(filel,fitskw='FLUX'):
    """
    make a lightcurve from a concatenated filel from kasoc which,
    for ONLY GOD KNOWS WHY FXXXING REASON the kasoc people
    made in a different way than the unconcatenated ones 
    (both in time offset and amplitude normalization)
    """
    fitsfile=pyfits.open(filel[0])
    
    t, d= np.array(fitsfile[1].data.field('TIME')), np.array(fitsfile[1].data.field(fitskw))
    
    qual_flags  = np.array(fitsfile[1].data.field('SAP_QUALITY'))
    
    idx = qual_flags == 0
    
    t, d = t[idx], d[idx]
    
    sort_idx = np.array(np.isinf(d) | np.isnan(d))
    
    t, d = t[np.invert(sort_idx)], d[np.invert(sort_idx)]
    
    fitsfile.close()
    
    return t, d


def ppm_to_mmag(light):
    """
       Convert ppm lightcurve to mmag (millimagnitudes) using the formula 

                 light(mmag)=-2.5*log_10(1-light(ppm)/1e6)*1e3

       found at
         https://www.aavso.org/converting-ppm-magnitudes-kepler-data
    """
    return -2.5 * np.log10(1-light/1e6) * 1e3


def rebin(rebin,x, y, dy = np.array([]) , method= 'Bresenham' ) :
# Use Bresenham's algorithm to rebin data into (almost) equal slices.
#  (see http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm)
    #Note type of input data, so same type can be returned

    #DEFINING METHODS
    def Bresenham (rebin,x,y,dy) :
         data_type = type(x)
         # convert input data to numpy array (does nothing if already true)
         x,y,dy = np.array(x),np.array(y),np.array(dy)
         # initialize rebinned data and error
         x_rebin, y_rebin =  [], []
         if len(dy) > 0 : 
             dy_rebin = []
         # initialize data index for beginning of current rebin
         bin_start = 0
         # initialize rebinned index+1
         rebin_index = 1
         while bin_start< len(y) :
             bin_end = min(int(round(rebin * rebin_index)),len(y))
             x_rebin.append(sum(x[bin_start:bin_end])/(bin_end-bin_start))
             y_rebin.append(sum(y[bin_start:bin_end]/(bin_end-bin_start)))
             # Uncertainties combined as square-root of sum of squares
             if len(dy) > 0 : 
                 dy_rebin.append((sum(dy[bin_start:bin_end]**2))**0.5/(bin_end-bin_start))
             bin_start= bin_end
             rebin_index += 1
         if data_type == np.ndarray :
             if len(dy) > 0 : 
                 return np.array(x_rebin), np.array(y_rebin), np.array(dy_rebin)
             else : 
                 return np.array(x_rebin), np.array(y_rebin)
         else :
             if len(dy) > 0 : 
                 return x_rebin, y_rebin, dy_rebin
             else : 
                 return x_rebin, y_rebin

    switch= { 'Bresenham' : Bresenham }

    return switch[method](rebin,x,y,dy)

def high_pass_filter (t, light, dt = 0.2, kind = 'cubic',k = 3,spline_out=False):
    """
      Apply a high pass filter by rebinning the lightcurve to a dt = 0.5 days(default)
      and then removing the resulting cubic-spline (default) of the rebinned lightcurve 
      from the original lightcurve
   
      N.B. Uses scipy.interpolate.UnivariateSpline for the interpolation
    """
#   from scipy.interpolate import interp1d
    from scipy.interpolate import UnivariateSpline
    n_bin = len(t[t-t[0] <= dt])
    t_rebin, l_rebin = rebin(n_bin,t,light)   
    t_rebin[0]=t[0]
    t_rebin[-1]=t[-1]
#   f=interp1d(t_rebin,l_rebin,kind=kind)
    f=UnivariateSpline(t_rebin,l_rebin,k=k)

    if spline_out :
        return light-f(t), f(t)
    else :
        return light-f(t)
    

def download_kepler(KIC,USER ='papini@mps.mpg.de',this_folder = False, verbose= False, path_save = [],table_file= None):
    """
       download fits files of a star identified by KIC='00XXXXX' selecting the quarters and dataset 
       from a table previously downloaded from kasoc.
       fits files are saved in a folder named "KIC00XXXXX'
       
       INPUT:
         KIC : string containing the KIC ID number of the star.

       OUTPUT:
         list containing the names of the downloaded files.
       OPTIONAL INPUT:
         verbose   = False   :(self explaining)
         this_folder = False : if set True download the fits file in the current working folder instead.
       OPTIONAL OUTPUT:
         path_save = []      :empty list to be replaced with the path where files are downloaded.

       
    """
    import numpy as np
    import os
    from getpass import getpass
    
    if table_file != None:
        file_name=table_file
    else:
        file_name=os.environ['SCRATCH']+'KEPLER/kasoc_tables/activity_table_slc_newest.txt'
    
    #KIC='003240411'
    
    #USER='papini@mps.mpg.de'
    
    
    dataset=np.loadtxt(file_name,delimiter=',',dtype='str')
    
    #selecting KIC
    kdata=np.where([KIC in s for s in dataset[:,0].flat])
    kdata=kdata[0]
    #selecting not working group
    kdata=kdata[np.where([ 'wg' not in s for s in dataset[kdata,0].flat])]
    
    #downloading fits files
    work_dir=os.getcwd() 
    if (not this_folder) :
        dummy=os.system('mkdir KIC'+KIC)
        os.chdir('./KIC'+KIC)
    path_save.append(os.getcwd())

    file_list=[ifile+'.fits' for ifile in dataset[kdata,0].flat]   
    if verbose :
        print([ifile+'.fits' for ifile in dataset[kdata,0].flat]) 
        print('\n...will be downloaded in :'+os.getcwd())        
        
    cookie='cookie.tt.txt'
    PSWD=getpass('Password for Kasoc:')
    wget_initialize="wget -q -O /dev/null --keep-session-cookies --save-cookies "+cookie+' --post-data "username='+USER+'&password='+PSWD+'&token=wget" '+"'http://kasoc.phys.au.dk/login.php' && chmod 0600 "+cookie
    
    os.system(wget_initialize)
    for i,ifile in enumerate(dataset[kdata,0]) :
        if not os.path.isfile(ifile+'.fits') :
            os.system("wget -nc -nv -erobots=off --load-cookies "+cookie+" -O '"+ifile+".fits' 'http://kasoc.phys.au.dk/download.php?fileid="+dataset[i,-3]+"&format=fits'")
        else : print('...file '+ifile +'.fits already exists! Skipping')    
    os.system('rm -f '+cookie) 
    os.chdir(work_dir)

    return file_list


